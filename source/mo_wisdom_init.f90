MODULE mo_wisdom_init
  !
  ! WISDOM.
  ! A. Tompkins 
  ! tompkins@ictp.it
  ! Date created: 04/2014
  !
  ! contains all the initialization routines
  !
  USE netcdf
  USE mo_domain
  USE mo_functions
  USE mo_wisdom_agent
  USE mo_wisdom_control
  USE mo_wisdom_constants
  USE mo_grid

  IMPLICIT NONE

!  PRIVATE
!  PUBLIC ::  wisdom_init_namelist ! sets up namelists
!  PUBLIC ::  wisdom_init_namelist ! sets up namelists

CONTAINS

  !-----------------------------------------------------------------------
  !
  ! SUBROUTINE:  wisdom_namelist
  ! wisdom setup does all the allocation
  !
  !-----------------------------------------------------------------------
  SUBROUTINE wisdom_init_namelist

    ! region namelist 
    NAMELIST /region/lon,nlon,dlon,lat,nlat,dlat

    ! *** TEMPORARY *** - will be moved to a common location
    !
    ! open the vectri namelist to get the region 
    !
    OPEN(8,file='/Users/tompkins/CODE/wisdom/wisdom/data/vectri.namelist',status='OLD')
    READ(8,nml=region)
    CLOSE(8) ! namelist file

    nxy=nlon*nlat  ! location is a single array -> use pointers to index it.

  END SUBROUTINE wisdom_init_namelist

  !-----------------------------------------------------------------------
  !
  ! SUBROUTINE:  wisdom_setup
  ! wisdom setup does all the allocation
  !
  !-----------------------------------------------------------------------
  SUBROUTINE wisdom_init_setup
    ALLOCATE (head(nxy))
    ALLOCATE (coord1(nxy),coord2(nxy))
    ALLOCATE (rpopdensity(nxy))
    ALLOCATE (rdist(nxy,nxy),rprobxy(nxy,nxy))
    ALLOCATE (wisdom_diag_nmoveaway(nxy),wisdom_diag_nmoveto(nxy))

    ! environmental grids
    ALLOCATE (cholera_b(nxy),cholera_delta_b(nxy),cholera_newcases(nxy))
    ALLOCATE (cholera_time(nxy))
    
    wisdom_diag_nmoveaway=0.0
    wisdom_diag_nmoveto=0.0
  END SUBROUTINE wisdom_init_setup
  
  !-----------------------------------------------------------------------
  !
  ! wisdom_init_maps
  !
  !-----------------------------------------------------------------------
  SUBROUTINE wisdom_init_maps
  
    INTEGER :: ixy1,ixy2
    REAL :: zspopi1,zspopi2 ! population density sum
    REAL :: zovernight ! overnight probability curve
    REAL, ALLOCATABLE :: rdistp(:,:)
    
    PRINT *,'%I: initializing maps '

    ALLOCATE (rdistp(nxy,nxy))

    !
    ! radiation model  alternative to gravity law
    ! Simini et al. NATURE 2012, doi 201.1038/nature10856
    ! note we use population density instead of total, but factor drops out.
    !
  
    rdist=0.0
  
    ! at teh moment this is crow flie distance - will be repalced by the node tracing algorithm,
    ! but that is SLOW O(nxy^4). so we will need to calculate once and save the result...
    ! or we can even load pre-calculated resistance maps.
    DO ixy2=1,nxy
      DO ixy1=ixy2+1,nxy ! diagonal matrix
        rdist(ixy1,ixy2)=rkmperdeg*SQRT((coord1(ixy1)-coord1(ixy2))**2+(coord2(ixy1)-coord2(ixy2))**2)
        rdist(ixy2,ixy1)=rdist(ixy1,ixy2)
      ENDDO
    ENDDO

    ! power law to speed up loop
    rdistp=rdist**1.0
    
    ! modified radiation law for the probability of movement:
    ! - take sqrt of the population, otherwise have r^4 power on bottom... this way is r^2.
    ! - rprobxy is the equivalent of Tij in Simini et al.
    ! - it is not proportional to  rpopdensity(ixy1) since this is the probability PER AGENT
    ! - thus the population density enters implicitly in the agent based use of this function

    ! need to tidy up all the if then 
    rprobxy=0.0
    DO ixy2=1,nxy    
      DO ixy1=1,nxy
        !zovernight=1.0-EXP(-rdist(ixy1,ixy2)/rovernighttau)
        zovernight=1.0
        IF (lradiation) THEN 
            ! first calculate distance between points.! **SUM** of population density within radius rd
            zspopi1=SUM(rpopdensity,MASK=rdist(:,ixy1)<=rdist(ixy1,ixy2))
            IF (zspopi1>1e-8) THEN
              rprobxy(ixy2,ixy1)=zovernight*rpopdensity(ixy1)* &
               & rpopdensity(ixy2)/ &
               & ((rpopdensity(ixy1)+zspopi1)* &
               & (rpopdensity(ixy1)+rpopdensity(ixy2)+zspopi1))
            ENDIF
          ELSE ! gravity
            ! slow!!!
            !zspopi1=SUM(rpopdensity,MASK=rdist(:,ixy1)<=gravity_radius)
            !zspopi2=SUM(rpopdensity,MASK=rdist(:,ixy2)<=gravity_radius)
            zspopi1=rpopdensity(ixy1)
            zspopi2=rpopdensity(ixy2)
            IF (rdist(ixy1,ixy2)>1e-8.and.rpopdensity(ixy1)>1.e-8 .and.rpopdensity(ixy2)>1.e-8) THEN 
              rprobxy(ixy2,ixy1)=zovernight*zspopi1*zspopi2/(rdistp(ixy1,ixy2)**4)             
            ENDIF
          ENDIF
      ENDDO
    ENDDO
    
    ! scale to make probability total unity.
    DO ixy1=1,nxy
      rprobxy(:,ixy1)=rprobxy(:,ixy1)/MAX(SUM(rprobxy(:,ixy1)),1.0e-20)
    ENDDO
  
    ! now sum up the probabilities to make the CDF...
    DO ixy1=1,nxy 
      DO ixy2=2,nxy ! index 2 since index 1 is untouched.
        rprobxy(ixy2,ixy1)=rprobxy(ixy2-1,ixy1)+rprobxy(ixy2,ixy1)
      ENDDO   
    ENDDO
  
    ! now clean up rounding error:
    WHERE (rprobxy(nxy,:)>0.5)
      rprobxy(nxy,:)=1.0
    ELSEWHERE
      rprobxy(nxy,:)=0.0
    ENDWHERE     
  
    ! nagent is simply the number of inhabited cells multipled by the agents per cell
    ! NOTE : MORE COMPLICATED ALGORITHM REQUIRED WHEN CHANGE TO IRREGULAR GRID. (then need to sum cells)
    nagent=npeocell*NINT(SUM(rprobxy(nxy,:)))

    print *,nagent,'agents have been initialized',npeocell,NINT(SUM(rprobxy(nxy,:)))
    
    DEALLOCATE(rdist,rdistp) ! save some memory

    ! health environment initialization
    cholera_b(:)=0.0
    cholera_b(1898)=1.0
    cholera_time(:)=0
    
  END SUBROUTINE wisdom_init_maps
  
  
  
  !
  ! end of module
  !
END MODULE mo_wisdom_init
  
