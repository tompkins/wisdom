MODULE mo_wisdom_agent
  !
  ! WISDOM - Adrian M Tompkins 2014
  !          tompkins@ictp.it
  !
  ! module for key methods on agents...
  !
  USE mo_domain
  USE mo_functions
  USE mo_wisdom_constants
  USE mo_wisdom_control
  USE mo_wisdom_health
  USE mo_grid
  
  IMPLICIT NONE
  
  ! diseases
  TYPE cholera
     INTEGER :: status  ! class status, 1=S, 2=I, 3=R
  END TYPE cholera

  TYPE flu
     INTEGER :: status  ! class status, 1=S, 2=E, 3=I, 4=R
  END TYPE flu

  TYPE malaria
     INTEGER :: status  ! class status, 1=S, 2=I, 3=R
     REAL :: load ! parasite load
  END TYPE malaria
  
  TYPE health  
    TYPE(cholera) :: cholera ! stuff
    TYPE(malaria) :: malaria ! stuff
    TYPE(flu) :: flu ! stuff
  END TYPE health

  TYPE agent
    ! location information - kind=2 only allows a 256x256 domain
    INTEGER(KIND=2) :: currloc  ! current location
    INTEGER(KIND=2) :: homeloc  ! home location
    INTEGER(KIND=2) :: regloc(nregloc)   ! regular visit location array e.g. birth place
        
    ! memory requirements:
    ! 1 element = 10x 4 bytes (integer)
    ! *250 *nx*ny = 40*250*100*100 = 100 million bytes = 100 MB...  oh, easy! 
    REAL    :: density  ! how many people agent represents

    ! ok this is how to handle age - people die each step, so all agents
    ! are reduced. Continuously need to inflate the
    ! YOUNGEST agent N for the new people being born  - at the end
    ! of the year the OLDEST agent group dies and the new one replaces it
    ! - we can also implement nonlinear ages ranges in order to save
    ! agents - i.e. we can have... no i think is best to record accurate
    ! age in years.
    ! general agent properties
    INTEGER :: age      ! mean age 
    INTEGER :: name     ! integer name
    REAL    :: wealth   ! gdp of agents.
    
    !REAL :: population ! pop density represented by agent
    TYPE(health) :: health
    
    TYPE(agent), POINTER :: prev=>null(),next=>null() ! to enable double-linked lists
  END TYPE agent

  !
  ! this is the list head
  !
  TYPE agent_ptr
    TYPE(agent), POINTER :: self=>null(),next=>NULL()
  END TYPE agent_ptr

  ! people list - this will not grow in time
  ! so we only need to allocate it once.
  ! if eventually we move to a one per one paradigm
  ! this will also need to be a linked list
  ! 
  TYPE(agent), ALLOCATABLE, TARGET :: people(:)
  
  !
  ! this is the head of the list for each gridcell:
  !
  TYPE(agent_ptr), ALLOCATABLE :: head(:)
  
CONTAINS ! methods on people
  
  !--------------------------------------
  ! insert a new person ip at location ixy
  !--------------------------------------
  SUBROUTINE wisdom_agent_insert(ip,ixy)
  
    IMPLICIT NONE
  
    INTEGER, INTENT(in) :: ip,ixy
  
    IF (associated(head(ixy)%next)) then 
      ! insert person at head - double linked list
      people(ip)%next=>head(ixy)%next ! i points to old head
      people(ip)%next%prev=>people(ip) ! old head points to new entry 
      people(ip)%prev=>null() ! disassociate prev as points to head
    ENDIF
    head(ixy)%next=>people(ip)
    people(ip)%currloc=ixy ! store the list location.
  END SUBROUTINE wisdom_agent_insert
  
  !---------------------------
  ! remove a person element ip
  !---------------------------
  SUBROUTINE wisdom_agent_remove(ip)
  
    INTEGER, INTENT(in) :: ip
  
    IF (ASSOCIATED(people(ip)%prev))then
      ! link prev to next directly
      people(ip)%prev%next=>people(ip)%next ! points to null if last entry
      IF (ASSOCIATED(people(ip)%next)) people(ip)%next%prev=>people(ip)%prev  
    ELSE
      ! previous is head, so need reset head pointer to new list head
      head(people(ip)%currloc)%next=>people(ip)%next ! point head to new list head
      IF (ASSOCIATED(people(ip)%next)) people(ip)%next%prev=>null() ! nullify prev as head
    ENDIF
    
  END SUBROUTINE wisdom_agent_remove
  
  !----------------------------------
  ! move agent ip from current to ixy
  !----------------------------------
  SUBROUTINE wisdom_agent_move(ip,ixy)
    INTEGER, INTENT(in) :: ip,ixy
    CALL wisdom_agent_remove(ip)
    CALL wisdom_agent_insert(ip,ixy)
  END SUBROUTINE wisdom_agent_move
  
  !--------------------------------
  ! return list characterists at ixy
  !--------------------------------
  SUBROUTINE wisdom_agent_collect
  
    USE mo_wisdom_control

    ! unsure whether to collect stats here,
    ! or simply keep track of stats during moves.
    ! need bulk stats to pass to vectri for vector biting and transmission
    ! 
    
    INTEGER :: ixy
    TYPE(agent), POINTER :: dummy
  
    DO ixy=1,nxy
      PRINT *,'-------------'
      PRINT *,'location ',ixy
      dummy=>head(ixy)%next  

      DO WHILE (ASSOCIATED(dummy)) 
        PRINT *,'index ',dummy%name
        dummy=>dummy%next
      ENDDO
    ENDDO
  
  END SUBROUTINE wisdom_agent_collect

  !-----------------------------------------------------------------------
  !
  ! wisdom_init_agent initialises all the agents
  !
  !-----------------------------------------------------------------------
  SUBROUTINE wisdom_agent_init
  
    ! local 
    INTEGER :: ipeo,ixy,index,iregloc,loc, iran
    REAL, ALLOCATABLE :: ranuni1(:), ranuni2(:)
    REAL :: ranuni
  
    ! do we have n people per agent or n agents per cell?
    ! i.e. should we have more agents in urban locations?
    ! 
  
    PRINT *,'%I: initializing agents '
  
    ! define nagent
    ALLOCATE (people(nagent))
    ALLOCATE (wisdom_diag_ntrips(nagent))
    wisdom_diag_ntrips=0

    !
    ! initialize agents
    !
    index=0 ! person name is a number (sorry!) 
  
    ! loop over cells
    ALLOCATE(ranuni1(nxy*npeocell*nregloc))
    ALLOCATE(ranuni2(nxy*npeocell))
    CALL RANDOM_NUMBER(ranuni1)
    CALL RANDOM_NUMBER(ranuni2)
    iran=1

    DO ixy=1,nxy
      ! first check point is not in the sea!
      IF (rprobxy(nxy,ixy)>0.5) THEN  
        DO ipeo=1,npeocell ! number of agents in each cell.
  
          index=index+1 ! increase index for next agent
  
          ! initialize age
          people(index)%age=-999  ! do later
          people(index)%density=rpopdensity(ixy)/REAL(npeocell) ! will change as people die...
          people(index)%name=index ! my integer name...

          ! initialize health
          people(index)%health%malaria%status=1  
          people(index)%health%cholera%status=1  

          ! infection in high pop density point only
          IF (ranuni2(index)<0.2.and.ixy==1898) THEN
            people(index)%health%flu%status=2
          ELSE
            people(index)%health%flu%status=1  
          ENDIF
         
          ! initialize location
          people(index)%currloc=ixy
          people(index)%homeloc=ixy ! some of these will be /= ixy to initialize agents away from home
          people(index)%regloc(:)=-999 ! initialize all regular locations to undefined.
      
          ! ok there is a PDF of people that have regular alternatives. 
          !    np=   pdfregloc-REAL(ipeo)/REAL(npeocell)
          DO iregloc=1,nregloc
            ranuni=ranuni1(iran)
            iran=iran+1
            loc=valloc(ranuni,rprobxy(:,ixy)) 
            people(index)%regloc(iregloc)=loc
          ENDDO
  
          ! set the probability vectors for migration
          ! this will be a DISTRIBUTION later, so that some people migration more often and to more reg places
          ! but for the moment this is simply the same for all.
          ! I suspect later I will make this a function of wealth. 
        !  people(index)%prob_home=rprob_mig_home
        !  people(index)%prob_away=rprob_mig_away
  
          CALL wisdom_agent_insert(index,ixy)  ! place the person in ixy 
  
        ENDDO
      ENDIF
    ENDDO
    DEALLOCATE(ranuni1,ranuni2)
    
    IF (index/=nagent) THEN
      PRINT *,'%E: error, agent number different to that expected'
      STOP
    ENDIF
  
    PRINT *,'%I: finished initializing ',index,' agents'
  
  END SUBROUTINE wisdom_agent_init


  SUBROUTINE wisdom_agent_cyclic_move
    !
    ! routine to move a number of people cyclic migration
    !
    REAL :: rannum(nagent), r1
    INTEGER :: ip, newloc, index
    !
    ! where to move to 
    ! Assume people have approx 2-4 regular locations (work, home, family), 
    ! to which they regular move to, and then some occassional one off locations (once per year). 
    ! divide the phone dataset into L=1, L>1 (more than once) - 
    ! need fraction of regular location visits to normal ones - 
    ! Then also need distances to regular and non-regular target locations.
    ! prob can then be higher for regular location, BUT how to sure most time is at home?
    ! PDN
    CALL RANDOM_NUMBER(rannum)
    DO ip=1,nagent
      IF (people(ip)%currloc==people(ip)%homeloc) THEN
!        use this one if the array in agent structure 
!        index=valloc(people(ip)%prob_mig_home) 
        index=valloc(rannum(ip),rprob_mig_home)
        SELECT CASE (index)
        CASE(1:nregloc)
          newloc=people(ip)%regloc(index) 
!         print *,'choosing reg ',index,newloc
        CASE(nregloc+1) ! random location based on location 
           CALL RANDOM_NUMBER(r1)
           newloc=valloc(r1,rprobxy(:,people(ip)%homeloc)) 
        CASE DEFAULT
          newloc=people(ip)%currloc
        END SELECT
      ELSE ! already away from home
!        use this one if prob array in agent structure
!        index=valloc(people(ip)%prob_mig_away) 
        index=valloc(rannum(ip),rprob_mig_away)
        SELECT CASE (index)
        CASE(1:nregloc)
          newloc=people(ip)%regloc(index)
        CASE(nregloc+1) ! random location
          CALL RANDOM_NUMBER(r1)
          newloc=valloc(r1,rprobxy(:,people(ip)%homeloc))
          print *,rprobxy(:,people(ip)%homeloc)
          stop
        CASE(nregloc+2) ! return home
          newloc=people(ip)%homeloc
        CASE DEFAULT
          newloc=people(ip)%currloc
        END SELECT
      ENDIF

     ! intervention for cholera - people may not move if they hear
     ! there is an outbreak
 !     IF (cholera_time(newloc)>cholera_newsdays) THEN
 !       CALL RANDOM_NUMBER(r1)
 !       IF (r1<cholera_nomovep) newloc=people(ip)%currloc
 !    ENDIF
     
      ! move if change of location
      IF (newloc/=people(ip)%currloc) THEN
        !------------
        ! diagnostics
        !------------
        wisdom_diag_nmoveaway(people(ip)%currloc)= &
          & wisdom_diag_nmoveaway(people(ip)%currloc)+people(ip)%density
        wisdom_diag_nmoveto(newloc)= &
          & wisdom_diag_nmoveto(newloc)+people(ip)%density
        IF (people(ip)%currloc==people(ip)%homeloc) &
          & wisdom_diag_ntrips(ip)=wisdom_diag_ntrips(ip)+1
        ! move the agent:
        CALL wisdom_agent_move(ip,newloc)       
      ENDIF
    ENDDO
  END SUBROUTINE wisdom_agent_cyclic_move

  !-----------------------------------------------------
  ! subroutine for permanent migration
  !-----------------------------------------------------
  SUBROUTINE wisdom_agent_migration
    !
    ! permanent agent migration
    !
  END SUBROUTINE wisdom_agent_migration

  !-----------------------------------------
  ! Health 
  !-----------------------------------------    
  SUBROUTINE wisdom_agent_health_cholera
    !
    ! routine to handle cholera SBIR model
    !
    ! cholera equations
    ! dS/dt = -beta B/(B+K)S + r R
    ! dI/dt = +beta B/(B+K)S - i I
    ! dR/dt = +iI - rR
    ! dB/dt = shed.I - I/relax
    IMPLICIT NONE
    REAL :: ran1(nagent),ran2,B_i,prob,homefac
    INTEGER :: ip,status,iloc

    ! shedding array reset
    cholera_delta_b(:)=0.0
    cholera_newcases(:)=0.0
    
    CALL RANDOM_NUMBER(ran1)
    
    DO ip=1,nagent
      status=people(ip)%health%cholera%status
      iloc=people(ip)%currloc
      IF (people(ip)%currloc==people(ip)%homeloc) THEN
         homefac=0.1
      ELSE
         homefac=1.0
      ENDIF
      SELECT CASE (people(ip)%health%cholera%status)
      CASE(1) ! susceptible    
        B_i=cholera_B(iloc)
        prob=B_i/(B_i+1.0)
        IF (ran1(ip)<prob) THEN
          CALL RANDOM_NUMBER(ran2)
          IF (ran2<h_chol_sigma) THEN
             status=2
             cholera_newcases(iloc)=cholera_newcases(iloc)+people(ip)%density
             if (cholera_time(iloc)==0) cholera_time=1
             ! mark new clinical case in output
          ELSE
             status=3
          ENDIF
        ENDIF
      CASE(2) ! clinical     
        IF (ran1(ip)<h_chol_infday) status=4
        ! shedding to environment - 1e8 temproal fudge to scale to people per cell
        cholera_delta_b(iloc)=cholera_delta_B(iloc)+h_chol_shedrate*people(ip)%density*homefac
      CASE(3) ! assympotimatic
        IF (ran1(ip)<h_chol_infday) status=4
        ! shedding to environment
        cholera_delta_b(iloc)=cholera_delta_b(iloc)+h_chol_shedrate*people(ip)%density*homefac
      CASE(4) ! immune
         IF (ran1(ip)<h_chol_immloss) status=1
      END SELECT

      ! reset new status
      people(ip)%health%cholera%status=status
    ENDDO
   
    
        
  END SUBROUTINE wisdom_agent_health_cholera

  SUBROUTINE wisdom_agent_health_malaria
    !
    ! routine to handle malaria SEIR model
    !

  END SUBROUTINE wisdom_agent_health_malaria


  SUBROUTINE wisdom_agent_health_hiv
    !
    ! routine to handle HIV transmission
    !

  END SUBROUTINE wisdom_agent_health_hiv

  SUBROUTINE wisdom_agent_health_flu
    !
    ! routine to handle HIV transmission
    !
    print *,'i am here'
  END SUBROUTINE wisdom_agent_health_flu


  SUBROUTINE wisdom_agent_health
    !
    ! call health routines
    !
    CALL wisdom_agent_health_cholera
  END SUBROUTINE wisdom_agent_health

END MODULE mo_wisdom_agent
