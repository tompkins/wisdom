MODULE mo_wisdom_netcdf
  
! netcdf methods
!
  USE netcdf
  USE mo_domain
  USE mo_functions
  USE mo_wisdom_agent
  USE mo_wisdom_control
  USE mo_wisdom_constants, ONLY : rprob_mig_home, rprob_mig_away

  IMPLICIT NONE

  PUBLIC

! ncids and indices for 2d output fields
  INTEGER, PARAMETER :: ncvar_ps=2
  INTEGER :: wisdom_ncvar_nmoveaway(ncvar_ps)       
  INTEGER :: wisdom_ncvar_nmoveto(ncvar_ps)       
  INTEGER :: wisdom_ncvar_cholera_newcases(ncvar_ps)       
  INTEGER :: wisdom_ncvar_cholera_b(ncvar_ps)       

  

CONTAINS ! methods on these arrays

  !-----------------------------------------------------------------------
  !
  ! netcdf READING
  !
  !-----------------------------------------------------------------------
  SUBROUTINE wisdom_init_input_netcdf
  
    INTEGER :: ivarid,ncid_pop,result,latdimid,londimid,ndims
    INTEGER :: icoord1, icoord2, index
    INTEGER :: dimids(2) ! hard wired
    REAL :: rpopdensity_FillValue
    REAL, ALLOCATABLE :: tpop(:,:) ! temporary pop array to get from netcdf file.
  
    !---------------------
    ! read population data
    !---------------------
    PRINT *,'%I: opening population ',ncpopufile
    result=NF90_OPEN(path=ncpopufile,mode=nf90_nowrite,ncid=ncid_pop)
    ! CALL check_latlon(ncid_pop,tlats,tlons)
  
    result=NF90_INQ_VARID(ncid_pop, "population", iVarId)
    result=NF90_INQUIRE_VARIABLE(ncid_pop, iVarId, ndims = ndims, dimids=dimids ) 

    ! read the 1st dim 
    result=NF90_INQUIRE_DIMENSION(ncid_pop,dimids(1),name=coord1_name,len=ncoord1)
    ALLOCATE(coord1s(ncoord1))
    result=NF90_GET_VAR(ncid_pop, dimids(1), coord1s ) 
  
    ! read the 2nd dim
    result=NF90_INQUIRE_DIMENSION(ncid_pop,dimids(2),name=coord2_name,len=ncoord2)
    ALLOCATE(coord2s(ncoord2))
    result=NF90_GET_VAR(ncid_pop, dimids(2), coord2s ) 

  !  ALLOCATE (rpopdensity(ncoord1,ncoord2))
    ALLOCATE (tpop(ncoord1,ncoord2)) ! allocate population...
    result=NF90_GET_ATT(ncid_pop, iVarId, "_FillValue", rpopdensity_FillValue)
    result=NF90_GET_VAR(ncid_pop, iVarId, tpop(:,:))
    result=NF90_CLOSE(ncid_pop)
  
    ! reshape the population and lat/lon files to long vectors:
    rpopdensity=RESHAPE(tpop,(/nxy/))
    WHERE(rpopdensity<0.0)
      rpopdensity=0.0
    ENDWHERE
    DO icoord2=0,ncoord2-1
    DO icoord1=0,ncoord1-1
      index=icoord1+icoord2*ncoord1+1
      coord1(index)=coord1s(icoord1+1)
      coord2(index)=coord2s(icoord2+1)
    ENDDO
    ENDDO
  
  END SUBROUTINE wisdom_init_input_netcdf
  
  !-----------------------------------------------------------------------
  !
  ! wisdom_output_netcdf
  !
  !-----------------------------------------------------------------------
  SUBROUTINE wisdom_init_output_netcdf

    INTEGER :: coord1dimid, coord2dimid, timedimid
    INTEGER :: coord1varid, coord2varid, timevarid
    INTEGER :: iday

    PRINT *,'%I: initializing output netcdf'

    CALL check(NF90_CREATE(path=ncout_file1,cmode=NF90_CLOBBER,ncid=ncidout1))

    ! define the dimensions
    CALL check(NF90_DEF_DIM(ncidout1, coord1_name, ncoord1, coord1dimid))
    CALL check(NF90_DEF_DIM(ncidout1, coord2_name, ncoord2, coord2dimid))
    CALL check(NF90_DEF_DIM(ncidout1, time_name, nf90_unlimited, timedimid))
  
    ! define the coordinate variables. They will hold the coordinate
    ! information, that is, the latitudes and longitudes. A varid is
    ! returned for each.
    CALL check(NF90_DEF_VAR(ncidout1, coord1_name, NF90_REAL, coord1dimid, coord1varid) )
    CALL check(NF90_DEF_VAR(ncidout1, coord2_name, NF90_REAL, coord2dimid, coord2varid) )
    CALL check(NF90_DEF_VAR(ncidout1, time_name, NF90_REAL, timedimid, timevarid) )
  
    ! Assign units attributes to coordinate var data. This attaches a
    ! text attribute to each of the coordinate variables, containing the
    ! units.
    CALL check( NF90_PUT_ATT(ncidout1, coord2varid, "units", "degrees") )
    CALL check( NF90_PUT_ATT(ncidout1, coord1varid, "units", "degrees") )
    CALL check( NF90_PUT_ATT(ncidout1, timevarid, "units", time_units) )

    ndiag2d_wisdom=0
  
    IF (.true.) &
    & CALL define_ncdf_output(ncidout1,wisdom_ncvar_nmoveaway,"journeys_out", &
    & "journeys out","per day",  ndiag2d_wisdom, (/coord1DimId, coord2DimID/)) ! time goes last
    IF (.true.) &
    & CALL define_ncdf_output(ncidout1,wisdom_ncvar_nmoveto,"journeys_to", &
    & "journeys to","per day",  ndiag2d_wisdom, (/coord1DimId, coord2DimID/)) ! time goes last

    IF (.true.) &
    & CALL define_ncdf_output(ncidout1,wisdom_ncvar_cholera_newcases,"cholera_cases", &
    & "cholera cases","per day",  ndiag2d_wisdom, (/coord1DimId, coord2DimID, timedimid/)) ! time goes last

    IF (.true.) &
    & CALL define_ncdf_output(ncidout1,wisdom_ncvar_cholera_b,"cholera_b", &
    & "normalized bacterial loading","",  ndiag2d_wisdom, (/coord1DimId, coord2DimID, timedimid/)) ! time goes last

    
    ! End define mode.!!!!!!!!
    CALL check(NF90_ENDDEF(ncidout1))
  
    ! Write the coordinate variable data. This will put the latitudes
    ! and longitudes of our data grid into the netCDF file.
    CALL check( nf90_put_var(ncidout1, coord1varid, coord1s) )
    CALL check( nf90_put_var(ncidout1, coord2varid, coord2s) )
    DO iday=1,nstep
      CALL check( nf90_put_var(ncidout1, timevarid,iday,start=(/iday/)))
    ENDDO  
  END SUBROUTINE wisdom_init_output_netcdf

  !-----------------------------------------------------
  ! Subroutine to write out the 2d fields.
  ! may move this to a netcdf function
  !-----------------------------------------------------
  SUBROUTINE wisdom_write_netcdf_2d
    REAL, ALLOCATABLE :: wis_rdiag(:,:)

    ALLOCATE (wis_rdiag(ncoord1,ncoord2))

    wis_rdiag=RESHAPE(wisdom_diag_nmoveaway,[ncoord1,ncoord2])/REAL(nstep)
    IF(.TRUE.) &
  & CALL check(nf90_put_var(ncidout1, wisdom_ncvar_nmoveaway(1), wis_rdiag))

    wis_rdiag=RESHAPE(wisdom_diag_nmoveto,[ncoord1,ncoord2])/REAL(nstep)
    IF(.TRUE.) &
  & CALL check(nf90_put_var(ncidout1, wisdom_ncvar_nmoveto(1), wis_rdiag))

    DEALLOCATE(wis_rdiag)
  END SUBROUTINE wisdom_write_netcdf_2d

  
  SUBROUTINE wisdom_write_ncdf_timeslice(itime)

    IMPLICIT NONE

    INTEGER, INTENT(in) :: itime

    REAL, ALLOCATABLE :: wis_rdiag(:,:)

    ALLOCATE (wis_rdiag(ncoord1,ncoord2))

    ! new cases
    wis_rdiag=RESHAPE(cholera_newcases,[ncoord1,ncoord2])    
    IF(.TRUE.) &
  & CALL check(nf90_put_var(ncidout1, wisdom_ncvar_cholera_newcases(1), wis_rdiag, start=(/1,1,itime/)))

    ! cholera loading
    wis_rdiag=RESHAPE(cholera_b,[ncoord1,ncoord2])
    IF(.TRUE.) &
    & CALL check(nf90_put_var(ncidout1, wisdom_ncvar_cholera_b(1), wis_rdiag, start=(/1,1,itime/)))

    DEALLOCATE(wis_rdiag)

  END SUBROUTINE wisdom_write_ncdf_timeslice

END MODULE mo_wisdom_netcdf

