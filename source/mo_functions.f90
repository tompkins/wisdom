MODULE mo_functions
  !
  ! generic useful functions for dates, vectors etc.
  !
  IMPLICIT NONE
  PRIVATE
  PUBLIC :: valloc
  PUBLIC :: check
  PUBLIC :: define_ncdf_output

CONTAINS

  INTEGER FUNCTION valloc(r,vec)
    IMPLICIT NONE
    REAL, INTENT(IN) :: r
    REAL, INTENT(IN) :: vec(:)

    ! local vars
    INTEGER:: nv
    REAL, ALLOCATABLE :: svec(:)

    nv=SIZE(vec)
    IF (r>MAXVAL(vec)) THEN
      valloc=nv+1
    ELSE
      ALLOCATE(svec(nv))
      svec=vec-r
      WHERE(svec<0.0) svec=1.e20
      valloc=MINLOC(svec,DIM=1)
    ENDIF 
  END FUNCTION valloc

  !
  ! 
  !
  SUBROUTINE check(status)

    USE netcdf

    INTEGER, INTENT(in) :: status

    IF (status /= nf90_noerr) THEN
      PRINT *,TRIM(NF90_STRERROR(status))
      STOP 'Bad NETCDF status'
    END IF

  END SUBROUTINE check

  !
  !
  !
  SUBROUTINE define_ncdf_output(ncid,ivarid,name,title,iunits,ndiag,idarray)

    USE netcdf

    ! flexible vector to set coordinate dimensions
    INTEGER, INTENT(in) :: ncid, idarray(:) 
    INTEGER, INTENT(inout) :: ivarid(2), ndiag
    CHARACTER (len=*), INTENT(IN) :: name, title, iunits

    CALL check(NF90_DEF_VAR(ncid, name, nf90_FLOAT, idarray , iVarID(1)))
    CALL check(NF90_PUT_ATT(ncid, iVarID(1), "title", title) )
    CALL check(NF90_PUT_ATT(ncid, iVarID(1), "units", iunits) )
!    CALL check(NF90_PUT_ATT(ncid, iVarID(1), "_FillValue", rfillvalue) )
    ndiag=ndiag+1   ! increase the diagnostic counter
    iVarid(2)=ndiag
  END SUBROUTINE define_ncdf_output


END MODULE mo_functions
