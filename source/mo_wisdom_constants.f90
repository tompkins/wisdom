MODULE mo_wisdom_constants

  !
  ! mo_wisdom data constants
  !
  
  INTEGER :: nstep=4*365 !365 ! number of timesteps - default will be overwritten by namelist

  ! initial number of people elements per cell - the average agent density 
  ! this will change over time of course:
  ! what we will do later is to have a 16 bit referencing system and then
  ! introduce finer resolution in urban locations, so that the location is spatially resolved.
  INTEGER, PARAMETER :: npeocell=200  ! 100 or more reasonable
  
  ! age parameters
  REAL :: life_expectancy=60.0    ! in years
  REAL :: death_rate, birth_rate  ! per thousand  


  ! mobility parameters !*NMC*
  REAL :: rovernighttau=62    ! e-folding decay in km for overnight stay probability 
  REAL :: rprobjourney=0.03   !*NMC*
  INTEGER, PARAMETER :: nregloc=3  ! maximum number of regular locations (keep small for agent memory).
  REAL :: gravity_radius=25.0 ! 25 km radius used for pop density in gravity law. Weights larger for extensive cities   
  
  ! These will be the BASE probability vectors for migration
  ! For the moment these are treated as a constant 
  ! later we will generalize these to make the PDF of probs a distribution of wealth 
  ! assuming wealthier people travel more (cyclic remember) and also to more regular locations.

  ! From home: probability divisions are reg1, reg2, reg3, random, upper->1 no migration.
!  REAL,DIMENSION(4) :: rprob_mig_home=[0.012,0.0147,0.0153,0.017]
  REAL,DIMENSION(4) :: rprob_mig_home=[0.001,0.002,0.003,0.003]

  ! From away: probability divisions are reg1, reg2, reg3, random, RETURN HOME, upper->1 no migration.
!  REAL,DIMENSION(5) :: rprob_mig_away=[0.012,0.0147,0.0153,0.017,0.4]
  REAL,DIMENSION(5) :: rprob_mig_away=[0.001,0.002,0.003,0.003,0.2]

  ! DUPLICATES
  REAL, PARAMETER :: rkmperdeg=111.0

  !
END MODULE mo_wisdom_constants
