MODULE mo_wisdom_health
!
! health wisdom methods on agents
!

  IMPLICIT NONE

  ! CHOLERA constants
  ! dS/dt = -beta B/(B+K)S + r R
  ! dI/dt = +beta B/(B+K)S - i I
  ! dR/dt = +i I - r R
  ! dB/dt = shed.I - I/relax

  REAL :: h_chol_immloss=1./600. ! r: loss of immunity in 1/days
  REAL :: h_chol_sigma=0.019     ! proportion that are assymptomatic
  REAL :: h_chol_infday=0.2      ! 1/days clinical case lasts
  REAL :: h_chol_shedrate=0.1       ! p/aK
  REAL :: h_chol_b_len=(1.0+1.0/5.0)  ! factor 1+1/ndays of bacteria lifetime in environment

  REAL,PARAMETER :: cholera_newsdays=30  ! time since first case
  REAL,PARAMETER :: cholera_nomovep=0.75 ! prob person changes mind if outbreak going on

  ! FLU parameters (SEIR)
  REAL :: h_flu_latency=3
  REAL :: h_flu_dur=4
  REAL :: h_immunity=100

  ! COVID-19 parameters (SEIQR)
  ! Some E->R (assymptomatic)
  ! Some I->R (mild cases) 
  ! Some I->Q->R (serious cases)
  REAL :: zdummy

  ! MALARIA



  ! DENGUE


  
  
  
  PUBLIC

END MODULE mo_wisdom_health
