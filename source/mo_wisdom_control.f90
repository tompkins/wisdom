MODULE mo_wisdom_control
  !
  ! arrays sizes and so on
  !
  INTEGER :: nagent ! total number of agents
  
  ! DUPLICATES - will be moved to joint module later:
  CHARACTER(len=*), PARAMETER :: input='/Users/tompkins/CODE/wisdom/wisdom/data/'
  REAL :: rpopdensity_FillValue
  INTEGER  :: ncid_pop
  CHARACTER, PARAMETER :: ncpopufile*100=input//'popufile.nc'
  REAL, ALLOCATABLE :: rpopdensity(:)
  INTEGER :: ncoord1,ncoord2

  ! coordinate array
  REAL, ALLOCATABLE :: coord1s(:),coord2s(:),coord1(:),coord2(:)
  CHARACTER(len=20) :: coord1_name, coord2_name, time_name="time"

  ! hardwired date for moment...  this will be read from rainfall/temperature files
  CHARACTER(len=*), PARAMETER :: time_units="days since 2000-01-01 00:00 UTC" 

  
  ! netcdf output - total number of 2d fields
  INTEGER :: ndiag2d_wisdom

  ! distance and journey arrays
  REAL, ALLOCATABLE :: rdist(:,:),rprobxy(:,:)

! diagnostics
  REAL, ALLOCATABLE :: wisdom_diag_nmoveaway(:),wisdom_diag_nmoveto(:) ! record number journeys
  INTEGER, ALLOCATABLE :: wisdom_diag_ntrips(:)

! netcdf stuff
  INTEGER :: ncidout1, ncidout2
  CHARACTER(len=*),PARAMETER :: ncout_file1="wisdom_domain.nc"
  CHARACTER(len=*),PARAMETER :: ncout_file2="wisdom_agent.nc"

! gravity or radiation model?  
  LOGICAL :: lradiation=.false.
  
END MODULE mo_wisdom_control
