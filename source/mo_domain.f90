MODULE mo_domain
!
! A. M. Tompkins tompkins@ictp.it
! Date created: 12/2014
!
! module to define the simulation domain for all modules: VECTRI, WISDOM 
!

! number of grid points (nxy=nlat*nlon)
INTEGER :: nlon, nlat, nxy

! domain location 
REAL :: lon,dlon,lat,dlat

END MODULE mo_domain
