MODULE mo_grid

  USE mo_wisdom_health
  USE mo_wisdom_control
  USE mo_domain
  
  IMPLICIT NONE
  
  ! define all variables on the grid
  ! suspect we will need to merge this with vectri's control module...

  REAL, ALLOCATABLE :: cholera_b(:), cholera_delta_b(:)
  REAL, ALLOCATABLE :: cholera_newcases(:), cholera_time(:)

CONTAINS
  
  ! This is where VECTRI will link in - it is a grid based code - and will handle the
  ! various updates for the hydrology, vectors and cholera loading for example
  SUBROUTINE update_grid

    INTEGER :: ixy

    DO ixy=1,nxy
      ! bacteria in the environment
      cholera_b(ixy)=(cholera_b(ixy)+cholera_delta_b(ixy))/h_chol_b_len
      ! time since first cases
      if (cholera_time(ixy)>0) cholera_time(ixy)=cholera_time(ixy)+1 
    END DO
   

  END SUBROUTINE update_grid

  
  
END MODULE mo_grid
