PROGRAM WISDOM
!
! WISDOM - Adrian M Tompkins 2014
!          tompkins@ictp.it
!
  USE mo_functions
  USE mo_wisdom_constants
  USE mo_wisdom_control
  USE mo_wisdom_init
  USE mo_wisdom_agent
  USE mo_wisdom_netcdf
  
  IMPLICIT NONE

  ! health controls
  LOGICAL :: lhiv=.false., lvectri=.false., lwisdom_init

  ! local integers
  INTEGER :: itime

  DO itime=1,nstep

    lwisdom_init=(itime==1)

    ! initialize if first step and idealized start
    IF (lwisdom_init) THEN
      CALL wisdom_init_namelist
      CALL wisdom_init_setup
      CALL wisdom_init_input_netcdf 
      CALL wisdom_init_output_netcdf 
      CALL wisdom_init_maps
      CALL wisdom_agent_init  ! load maps and initialize age structure.
    ENDIF

    PRINT *,'step ',itime

    ! CALL wisdom_agent_birth()   ! agent are born
    ! CALL wisdom_agent_income()  ! agent wealth development
    CALL wisdom_agent_cyclic_move    ! cyclic migration
    ! CALL wisdom_agent_migration    ! permanent migration
    ! CALL wisdom_agent_death()   ! agent may die

    ! collect agent - needed for vectors/bites per person...  
    ! collecting is slow... why not stack agent while moving them... 
    ! CALL wisdom_agent_collect??? 
    
    ! health modules to update health status
    CALL wisdom_agent_health     


    ! call to VECTRI here:
    CALL update_grid

    ! netcdf write timeslice 
    CALL wisdom_write_ncdf_timeslice(itime)

  ENDDO ! loop

  CALL wisdom_write_netcdf_2d
  CALL check(NF90_CLOSE(ncidout1))

  print *,'%I: mean number of trips = ',REAL(SUM(wisdom_diag_ntrips))/REAL(nagent)

END PROGRAM WISDOM

