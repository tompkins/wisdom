MODULE mo_wisdom_health
!
! health wisdom methods on agents
!

  IMPLICIT NONE

  ! cholera constants
  ! dS/dt = -beta B/(B+K)S + r R
  ! dI/dt = +beta B/(B+K)S - i I
  ! dR/dt = +iI - rR
  ! dB/dt = shed.I - I/relax

  REAL :: h_chol_rec ! recovery rate
  REAL :: h_chol_inf ! recovery rate

  REAL :: flu_latency=3
  REAL :: flu_dur=4
  REAL :: immunity=100
  
  PUBLIC

END MODULE mo_wisdom_health
